Vue.component('day_timetable', {
    props: ['day'],
    template: '<div class="WeekDay">' +
        '<h3>{{day.day_name}}</h3>' +
        '<h3 v-if="day.lessons.length == 0" align="center">Выходной!</h3>' +
        '<lesson_block v-else v-for="(lesson, index) in day.lessons" v-bind:lesson="lesson" v-bind:key="index"></lesson_block>' +
        '</div>'
})

Vue.component('lesson_block', {
    props: ['lesson'],
    template: '<div class="Lesson" ' +
        'v-bind:class="{ ' +
        'ImportantLesson: lesson.importance == \'Важная\',' +
        'UselessLesson: lesson.importance == \'Бесполезная\', ' +
        'OddWeek: lesson.week_type_id == 1,' +
        'EvenWeek: lesson.week_type_id == 2 }" >' +
        '<p>{{lesson.lesson_name}}</p>' +
        '</div>'
})

Vue.component('week_type_peek', {
    props: ['week_types'],
    data: function () {
        return {
            selected_week_type: null,
            current_week: null
        }
    },
    watch: {
        selected_week_type: function (newWeekType) {
            this.$emit('week_type_changed', newWeekType);
        }
    },
    template: '<select v-model="selected_week_type">' +
        '<option v-for="week_type in week_types" v-bind:value="week_type.week_type_id">{{week_type.week_type}}</option>' +
        '</select>',
    mounted() {
        axios
            .post('/API/GetCurrentWeek.php')
            .then(response => (
                this.selected_week_type = response.data,
                this.current_week = response.data))

    }
})

new Vue({
    el: '#Wrapper',
    data: {
        days: null,
        week_types: null,
    },
    methods: {
        week_type_changed: function(newWeekType){
            axios
                .post('/API/GetTimeTable.php?week=' + newWeekType)
                .then(response => (this.days = response.data));
        }
    },
    mounted() {
        axios
            .post('/API/GetWeekTypes.php')
            .then(response => (this.week_types = response.data))
    }
})