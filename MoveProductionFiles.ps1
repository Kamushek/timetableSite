Set-Location -Path D:\OpenServer\OpenServer\domains
Remove-Item timetable6374\* -Recurse
Copy-Item -Path timetable\dist -Destination timetable6374 -Recurse
Copy-Item -Path timetable\API -Destination timetable6374 -Recurse
Copy-Item -Path timetable\favicon.ico -Destination timetable6374 -Recurse
Copy-Item -Path timetable\index.css -Destination timetable6374 -Recurse
Copy-Item -Path timetable\index.html -Destination timetable6374 -Recurse
echo "All files were removed.";
