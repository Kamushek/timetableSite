<?php

class Timetable
{

  private $currentTimetable = 0;
  private $daySchedules = array();
  private $database;

  public function Timetable($database)
  {
    $this->database = $database;
  }

  private function PrintError($methodName)
  {
    echo "---[ERROR " . $methodName . " was failed]---";
  }

  private function GetCurrentTimetable()
  {
    $currentDate = date('Y-m-d');
    $query = $this->database->Query("Select * from `Schedules` where Start_Date<'" . $currentDate . "' and Expired_Date>'" . $currentDate . "' and  Group_Id='1'");
    if ($query == false)
      $this->PrintError("GetCurrentTimetable");
    else {
      return $query->fetch_assoc()['Id'];
    }
  }

  private function GetAllDays()
  {
    $query = $this->database->Query("Select * from Weekdays");
    if ($query == false)
      $this->PrintError("GetAllDays");
    else {
      return $query;
    }
  }

  private function GetDaySchedule($day_id, $day, $week_Type_Id)
  {
    if ($week_Type_Id != 3)
      $weekCheckingString = "and (Week_Type_Id=" . $week_Type_Id . " or Week_Type_Id=3)";
    else
      $weekCheckingString = "";

    $query = $this->database->Query("select s.Id, Week_Type, Week_Type_Id, Weekday, Lesson, Importance, Lesson_Time_Id, Start_Time, Duration, Lesson_Type, Room, Firstname, Lastname, Middlename 
		from schedule as s 
		left join Week_Types as wt on s.Week_Type_Id=wt.Id 
		left join Weekdays as wd on s.Weekday_Id=wd.Id 
		left join Lessons as l on s.Lesson_Id=l.Id 
		left join Lesson_Times as lt on s.Lesson_Time_Id=lt.Id 
		left join Lesson_Types as lty on s.Lesson_Type_Id=lty.Id 
		left join Rooms as r on s.Room_Id=r.Id 
		left join Teachers as t on s.Teacher_Id=t.Id
		left join Importances as imp on s.Importance_Id=imp.Id
		where Schedule_Id=" . $this->currentTimetable . " 
		and Weekday_Id=" . $day_id . " "
      . $weekCheckingString .
      " order by Start_Time;");
    if ($query == false)
      $this->PrintError("GetDaySchedule");
    else {
      return new DaySchedule($query, $day);
    }
  }

  public function GetSchedule($week_Type_Id)
  {
    $this->currentTimetable = $this->GetCurrentTimetable();
    $days = $this->GetAllDays();
    for ($i = 0; $i < $days->num_rows; $i++) {
      $day = $days->fetch_assoc();
      $daySchedule = $this->GetDaySchedule($day['Id'], $day['Weekday'], $week_Type_Id);
      if ($daySchedule->lessons_count > 0)
        $this->daySchedules[] = $daySchedule;
    }
    return $this->daySchedules;
  }
}
?>
