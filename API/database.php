<?php

class dataBase
{
  private $host = '127.0.0.1';
  private $username = 'root';
  private $password = 'root';
  private $database = 'timetable_test'; //develop
  //private $database = 'timetable'; //production

  private $mysqli;

  function __construct()
  {
  }

  function Connect()
  {
    $this->mysqli = new mysqli($this->host, $this->username, $this->password, $this->database);
    if ($this->mysqli->connect_errno)
      return false;
    return true;
  }

  function Close()
  {
    $this->mysqli->close();
  }

  function Query($query)
  {
    //echo "[ ".$query." ]";
    return $this->mysqli->query($query);
  }
}
?>
