<?php

class DaySchedule
{

  public $lessons = array();
  public $day_name;
  public $lessons_count;

  public function DaySchedule($query, $day)
  {
    $this->lessons_count = $query->num_rows;
    $this->day_name = $day;
    for ($i = 0; $i < $this->lessons_count; $i++) {
      $this->lessons[] = new Lesson($query);
    }
  }
}

?>
