<?php

include "Boostraper.php";

$week = (date('W') % 2 + 1);

$date = new DateTime(date('Y-M-d'));
$date = $date->setTime(23, 59, 59);

header("Access-Control-Allow-Origin: " . $domain);
header('Expires: ' . $date->format('D, d M Y H:i:s') . ' GMT');

echo json_encode($week, JSON_UNESCAPED_UNICODE);
?>
