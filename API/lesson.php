<?php

class Lesson
{

  public $week_type;
  public $week_type_id;
  public $lesson_name;
  public $start_time;
  public $importance;
  public $duration;
  public $lesson_type;
  public $room;
  public $teacher;
  public $lesson_time_id;

  private function NormalTimeToISO8601($normal_time)
  {
    $time = preg_split("*:*", $normal_time);
    return "P0000-00-00T" . $normal_time;
  }

  public function Lesson($query)
  {
    $data = $query->fetch_assoc();
    $this->week_type = $data['Week_Type'];
    $this->importance = $data['Importance'];
    $this->week_type_id = $data['Week_Type_Id'];
    $this->lesson_name = $data['Lesson'];
    $this->lesson_time_id = $data['Lesson_Time_Id'];
    $this->start_time = $data['Start_Time'];
    $this->duration = $data['Duration'];
    $this->lesson_type = $data['Lesson_Type'];
    $this->room = $data['Room'];
    $this->teacher = new Teacher($data);
  }
	
}

?>
